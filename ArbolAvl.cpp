#include <iostream>
#include <unistd.h>
#include <fstream>
#include "ArbolAvl.h"

using namespace std;

ArbolAvl::ArbolAvl(){
}

Nodo* ArbolAvl::crear_nodo(string dato){
	Nodo *tmp;
    tmp = new Nodo;
	tmp->id = dato;
	tmp->izq = NULL;
	tmp->der = NULL;
	tmp->FactorE = 0;
	return tmp;
}

bool ArbolAvl::buscar_nodo(Nodo *tmp, string eliminar){
	if(eliminar < tmp->id){
		if(tmp->izq == NULL){
			cout << "No está el id" << endl;
			return false;
		}
		else{
			buscar_nodo(tmp->izq, eliminar);
		}
	}
	else{
		if(eliminar > tmp->id){
			if(tmp->der == NULL){
				cout << "No está el id" << endl;
				return false;
			}
			else{
				buscar_nodo(tmp->der, eliminar);
			}
		} 
		else{
			cout << "Se encontró el id a eliminar" << endl;
			return true;
		}
	}
	return true;
}


void ArbolAvl::insertar_nodo(Nodo *&tmp, string dato, bool &altura){
	if(tmp != NULL){
		if(dato.compare(tmp->id) < 0){
			insertar_nodo(tmp->izq, dato, altura);
			if(altura == true){
				switch(tmp->FactorE){
					case 1:
						tmp->FactorE = 0;
						altura = false;
						break;
					case 0:
						tmp->FactorE = -1;
						break;
					case -1:
						nodo1 = tmp->izq;
						if(nodo1->FactorE <= 0){
							tmp->izq = nodo1->der;
							nodo1->der = tmp;
							tmp->FactorE = 0;
							tmp = nodo1;
						}
						else{
							nodo2 = nodo1->der;
							tmp->izq = nodo2->der;
							nodo2->der = tmp;
							nodo1->der = nodo2->izq;
							nodo2->izq = nodo1;
							if(nodo2->FactorE == -1){
								tmp->FactorE = 1;
							}
							else{
								tmp->FactorE = 0;
							}
							tmp = nodo2;
						}
						tmp->FactorE = 0;
						altura = false;
						break;
					}
				}
			}
			else{
				if(dato.compare(tmp->id) > 0){
					insertar_nodo(tmp->der, dato, altura);
					if(altura == true){
						switch(tmp->FactorE){
							case -1:
								tmp->FactorE = 0;
								altura = false;
								break;
							case 0:
								tmp->FactorE = 1;
								break;
							case 1:
								nodo1 = tmp->der;
								if(nodo1->FactorE >= 0){
									tmp->der = nodo1->izq;
									nodo1->izq = tmp;
									tmp->FactorE = 0;
									tmp = nodo1;
								}
								else{
									nodo2 = nodo1->izq;
									tmp->der = nodo2->izq;
									nodo2->izq = tmp;
									nodo1->izq = nodo2->der;
									nodo2->der = nodo1;
									if(nodo2->FactorE == 1){
										tmp->FactorE = -1;
									}
									else{
										tmp->FactorE = 0;
									}
									if(nodo2->FactorE == -1){
										nodo1->FactorE = 1;
									}
									else{
										nodo1->FactorE = 0;
									}
									tmp = nodo2;
								}
								tmp->FactorE = 0;
								altura = false;
								break;
						}
					}
				}
				else{
					cout << " Id agregado previamente " << endl;
				}
			}
		}
		else{
			tmp = crear_nodo(dato);
			altura = true;
			cout << "El id fue agregado con exito"<< endl;
		}
}


void ArbolAvl::reestructuraIzq(Nodo *&tmp, bool &altura){
	if(altura == true){
		switch(tmp->FactorE){
			case -1:
				tmp->FactorE = 0;
				break;
			case 0:
				tmp->FactorE = 1;
				altura = false;
				break;
			case 1:
				nodo1 = tmp->der;
				if(nodo1->FactorE >= 0){
					tmp->der = nodo1->izq;
					nodo1->izq = tmp;
					if(nodo1->FactorE == 0){
						tmp->FactorE = 1;
						nodo1->FactorE = -1;
                        altura = false;
                    }
                    if(nodo1->FactorE == 1){
                        tmp->FactorE = 0;
                        nodo1->FactorE = 0;
                    }
                    tmp = nodo1;
				}
				else{
					nodo2 = nodo1->izq;
					tmp->der = nodo2->izq;
					nodo2->izq = tmp;
					nodo1->izq = nodo2->der;
					nodo2->der = nodo1;
					if(nodo2->FactorE == 1){
						tmp->FactorE = -1;
					}
					else{
						tmp->FactorE = 0;
					}
					if(nodo2->FactorE == -1){
						nodo1->FactorE = 1;
					}
					else{
						nodo1->FactorE = 0;
					}
					tmp = nodo2;
					nodo2->FactorE = 0;
				}
				tmp->FactorE = 0;
				altura = false;
				break;
			}
		}
}

void ArbolAvl::reestructuraDer(Nodo *&tmp, bool &altura){
	if(altura == true){
		switch(tmp->FactorE){
			case 1:
				tmp->FactorE = 0;
				break;
			case 0:
				tmp->FactorE = -1;
				altura = false;
				break;
			case -1:
				nodo1 = tmp->izq;
				if(nodo1->FactorE <= 0){
					tmp->izq = nodo1->der;
					nodo1->der = tmp;
					if(nodo1->FactorE == 0){
						tmp->FactorE = -1;
						nodo1->FactorE = 1;
						altura = false;
					}
					if(nodo1->FactorE == -1){
						tmp->FactorE = 0;
						nodo1->FactorE = 0;
					}
					tmp = nodo1;
				}
				else{
					nodo2 = nodo1->der;
					tmp->izq = nodo2->der;
					nodo2->der = tmp;
					nodo1->der = nodo2->izq;
					nodo2->izq = aux;
					if(nodo2->FactorE == -1){
						tmp->FactorE = 1;
					}
					else{
						tmp->FactorE = 0;
					}
					if(nodo2->FactorE == 1){
						nodo1->FactorE = -1;
					}
					else{
						nodo1->FactorE = 0;
					}
					tmp = nodo2;
					nodo2->FactorE = 0;
				}
		}
	}
}


void ArbolAvl::eliminar_nodo(Nodo *&tmp, string dato, bool &altura){
	if(tmp != NULL){
		if(dato.compare(tmp->id) < 0){
			eliminar_nodo(tmp->izq, dato, altura);
			reestructuraIzq(tmp, altura);
		}
		else{
			if(dato.compare(tmp->id) > 0){
				eliminar_nodo(tmp->der, dato, altura);
				reestructuraDer(tmp, altura);
			}
			else{
				aux = tmp;
				if(aux->der == NULL){
					tmp = aux->izq;
					altura = true;
				}
				else{
					if(aux->izq == NULL){
						tmp = aux->der;
						altura = true;
					}
					else{
						aux1 = tmp->izq;
						altura = false;
						while(aux1->der != NULL){
							aux2 = aux1;
							aux1 = aux1->der;
							altura = true;
						}
						tmp->id = aux1->id;
						aux = aux1;
						if(altura == true){
							aux2->der = aux1->izq;
						}
						else{
							tmp->izq = aux1->izq;
						}
						reestructuraDer(tmp->izq, altura);
					}
				}
				delete aux;
			}
		}
	}
	else{
		cout << "El id no existe" << endl;
	}
			
}


// Imprimir el arbol en inorden
void ArbolAvl::inorden(Nodo *tmp){
	if(tmp != NULL){
		inorden(tmp->izq);
		cout << "[" << tmp->id << "]" << "->";
		inorden(tmp->der);
	}
};

