#ifndef NODO_H
#define NODO_H

//se define estructura para nodo
typedef struct _Nodo{
    // Se cambia a formato string además de agregar el Factor de equilibrio
	string id = "\0";
    int FactorE = 0;
	struct _Nodo *izq;
	struct _Nodo *der;
} Nodo;
#endif