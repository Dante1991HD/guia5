#include <iostream>
#include <unistd.h>
#include <fstream>
#include "Grafo.h"
using namespace std;

// Clase para crear el grafico del arbol que recibe la raiz del arbol como parametro
Grafo::Grafo(Nodo *raiz){
	//creacion de archivo.txt
	ofstream archivo("grafo.txt", ios::out);
	// Primero debemos intentar abrir el archivo para evitar errores
	if (archivo.fail()){
		cout << "**No se pudo abrir archivo**" << endl;
	}
	else{
		// Escritura en el archivo del arbol
		archivo << "digraph G {\n";
		archivo << "node [style=filled fillcolor=red];\n";
		archivo << "nullraiz[shape=point];" << endl;
    	archivo << "nullraiz->\"" << raiz->id << "\" [label=" << raiz->FactorE << "];" << endl;
		// llamada a metodo para ingresar informacion del arbol
		recorrer_arbol(raiz, archivo);	
		archivo << "}";
		archivo.close();
		// Generacion de la imagen a traves del archivo de texto
		system("dot -Tpng -ografo.png grafo.txt &");
		// Apertura de la imagen
		system("eog grafo.png &");
	}	
}

// Metodo que recorre el arbol y crea el archivo.txt
void Grafo::recorrer_arbol(Nodo *tmp, ofstream &archivo){
	string cadena = "\0";
	if(tmp != NULL){
		if(tmp->izq != NULL){
			archivo << "\"" << tmp->id << "\"->\"" << tmp->izq->id;
			archivo << "\"[label=\"" << tmp->izq->FactorE << "\"]" << endl;
		}
		else{
			cadena = tmp->id + "i";
			archivo << "\"" << cadena << "\" [shape=point]; " << endl;
			archivo << "\"" << tmp->id << "\"->" << "\"" << cadena << "\";" << endl;
		}
		if(tmp->der != NULL){
			archivo << "\"" << tmp->id << "\"->\"" << tmp->der->id;
			archivo << "\"[label=\"" << tmp->der->FactorE << "\"];" << endl; 
		}
		else{
			cadena = tmp->id + "d";
			archivo << "\"" << cadena << "\" [shape=point]; " << endl;
			archivo << "\"" << tmp->id << "\"->" << "\"" << cadena << "\";" << endl;
		}
		// Luego se recorre en izquierda y derecha
		recorrer_arbol(tmp->izq, archivo);
		recorrer_arbol(tmp->der, archivo);
	}
}