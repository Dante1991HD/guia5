#include <iostream>
#include <unistd.h>
#include <fstream>
#include <vector>
#include "ArbolAvl.h"
#include "Grafo.h"

using namespace std;

int Menu() {
  int Op;
  
  do {
    printf("\n--------------------\n");
    printf("1) Insertar\n");
    printf("2) Eliminar\n");
    printf("3) Modificar\n");
    printf("4) Buscar\n");
    printf("5) Grafo\n");
    printf("6) Insertar 170.000 ids de proteinas\n");
    printf("0) Salir\n\n");
    printf("Opción: ");
    scanf("%d*c", &Op);
  } while (Op<0 || Op>7);
  
  return (Op);
}

int main(int argc, char* argv[]){
  int opcion;
  bool on = true;
  string dato;
  Nodo *raiz = NULL;
  ArbolAvl *arbol = NULL;
  Grafo *grafo;
  system("clear");
  bool inicio = false;

  while (on) {
    
    switch(opcion) {
      case 1:
        cout << "Ingresar elemento: ", getline(cin, dato);
        getline(cin, dato);
        inicio=false;
        arbol->insertar_nodo(raiz, dato, inicio);
        break;

      case 2:
        cout << ("Eliminar elemento: "), getline(cin, dato);;
        getline(cin, dato);
        inicio=false;
        arbol->eliminar_nodo(raiz, dato, inicio);
        break;
      
      case 3:
        cout << ("Modificar elemento: ");
        arbol->inorden(raiz);
        getline(cin, dato);
        inicio=false;
        arbol->eliminar_nodo(raiz, dato, inicio);
        cout << ("Ingresar elemento en su lugar: ");
        getline(cin, dato);
        inicio=false;
        arbol->insertar_nodo(raiz, dato, inicio);
        break;
      
      case 4:
        cout << ("Buscar elemento: ");
        getline(cin, dato);
        arbol->buscar_nodo(raiz, dato);
        break;
      case 5:
        cout <<("Generar grafo: ");
        grafo = new Grafo(raiz);

        break;
      case 6:
        fstream archivo;
        vector <string> nombre_archivo = {"rcsb_pdb_ids_1.txt", 
        "rcsb_pdb_ids_2.txt", "rcsb_pdb_ids_3.txt", "rcsb_pdb_ids_4.txt",
        "rcsb_pdb_ids_5.txt","rcsb_pdb_ids_6.txt", "rcsb_pdb_ids_7.txt"};
        string tmp;
          for(int i = 0; i < nombre_archivo.size(); i++) {
              archivo.open(nombre_archivo[i], ios::in);
                while(getline(archivo, tmp)){
                  arbol->insertar_nodo(raiz, tmp, inicio);
            }
        }
          break;
    }
    opcion = Menu();
  }
  
  return 0;
}